package org.jgi.comparative.cient;

import javax.persistence.EntityManager;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jgi.comparative.map.framework.EntityManagerKeeper;
import org.jgi.comparative.map.pph.PphRequest;

public class Enhancer {
	public Enhancer(){}
	@XmlTransient
	private int hs_chrom;
	public Enhancer(int hsChrom, int hsStart) {
		hs_chrom = hsChrom;
		hs_start = hsStart;
		provideUrl();
	}
	
	@XmlTransient
	private int hs_start;
	public String url;
	private final static String schemaName = PphRequest.enhancerSchema;
	
	private void provideUrl() {
		EntityManager em = EntityManagerKeeper.getSession();
		Session session = (Session) em.getDelegate();
		String queryString = "SELECT e.experiment_id " +
				"FROM " + schemaName + ".Locus l " +
				"INNER JOIN " + schemaName + ".Experiment e ON l.organism_id=e.organism_id " +
					"AND l.experiment_id=e.experiment_id " +
				"WHERE e.organism_id=1 AND is_internal=0 " +
					"AND hs_chrom = ? AND hs_start < ? AND hs_end > ?";
		Integer experiment_id = null;
		//try {
			Query query = session.createSQLQuery(queryString).setInteger(0,
					hs_chrom).setInteger(1, hs_start).setInteger(2, hs_start);

			experiment_id = (Integer) query.uniqueResult();
		/*} catch (HibernateException e) {
			// TODO: handle exception
			//remove try-catch block after enhancer db fixed
			ByteArrayOutputStream ba = new ByteArrayOutputStream();
			e.printStackTrace(new PrintStream(ba));
			System.out.println( ba.toString().substring(1, 300) );
		}*/
		if (experiment_id == null) {
			return;
		}
		url = PphRequest.enhancerUrl + 
			"?form=presentation&show=1&organism_id=1&experiment_id=" + experiment_id;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
