package org.jgi.comparative.cient;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class PolyPhenOutput implements Comparable<String> {
	
	public PolyPhenOutput(){}
	
	public PolyPhenOutput(String line, Map<Integer, String> hmap){
		if ( line == null || line.isEmpty() )
			throw new RuntimeException("-- ERROR: parsing failed. " + line);
		String[] fields = line.split("\\t");
		if ( hmap.size() < fields.length )
			throw new RuntimeException("-- ERROR: parsing failed. Incorrect number of fields. " + line);
		Map<String, String> entry = new LinkedHashMap<String, String>();
		for (int i=0; i< fields.length; i++) {
			String value = fields[i].trim(); 
			if ( value.equals("?") || value.isEmpty() )
				continue;
			entry.put(hmap.get(i), value);
		}
		o_snp_id = entry.get("o_snp_id");
		snp_id = entry.get("snp_id");
		acc = entry.get("acc");
		pos = Integer.valueOf(entry.get("pos"));
		aa1 = entry.get("aa1").charAt(0);
		aa2 = entry.get("aa2").charAt(0);
		prediction = entry.get("prediction");
		pph2_prob = Double.valueOf(entry.get("pph2_prob"));
		pph2_FPR = Double.valueOf(entry.get("pph2_FPR"));
		pph2_TPR = Double.valueOf(entry.get("pph2_TPR"));
	}	
	
	public String o_snp_id;
	public String snp_id;
	public String acc;
	public int pos;
	@XmlTransient
	public char aa1;
	@XmlElement(name="aa1")
	public void setAa1AsString(String s) {
	}

	public String getAa1AsString() {
		return String.valueOf(aa1);
	}

	@XmlTransient
	public char aa2;

	@XmlElement(name = "aa2")
	public void setAa2AsString(String s) {
	}

	public String getAa2AsString() {
		return String.valueOf(aa2);
	}

	public String prediction;
	public double pph2_prob;
	public double pph2_FPR;
	public double pph2_TPR;
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	@Override
	public int compareTo(String o) {
		return snp_id.compareTo(o);
	}

}
