package org.jgi.comparative.cient;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class PphAnnot {
	public PphAnnot(){}
	public PphAnnot(String line, Map<Integer, String> hmap){
		if ( line == null || line.isEmpty() )
			throw new RuntimeException("-- ERROR: parsing failed. " + line);
		String[] fields = line.split("\\t");
		if ( hmap.size() < fields.length )
			throw new RuntimeException("-- ERROR: parsing failed. Incorrect number of fields. " + line);
		Map<String, String> entry = new LinkedHashMap<String, String>();
		for (int i=0; i< fields.length; i++) {
			String value = fields[i].trim(); 
			if ( value.equals("?") || value.isEmpty() )
				continue;
			entry.put(hmap.get(i), value);
		}
		
		snp_pos = entry.get("snp_pos");
		strand = entry.get("str");
		gene = entry.get("gene");
		transcript = entry.get("transcript");
		refa = entry.get("refa");
		type = entry.get("type");
		nt1 = entry.get("nt1").charAt(0);
		nt2 = entry.get("nt2").charAt(0);
		dbrsid = entry.get("gene");  
	}
	
	public String snp_pos;
	public String strand;
	public String gene;

	public String transcript;
	public String refa;
	@XmlTransient
	public char nt1;
	@XmlTransient
	public char nt2;
	@XmlElement(name = "nt1")
	public void setNt1AsString(String s) {}
	public String getNt1AsString() {
		return String.valueOf(nt1);
	}
	@XmlElement(name = "nt2")
	public void setNt2AsString(String s) {}
	public String getNt2AsString() {
		return String.valueOf(nt2);
	}
	
	public String type;
	public String dbrsid;
	
	/*public String snp_pos;
	
	public PphAnnot(String line){
		if ( line == null || line.isEmpty() )
			throw new RuntimeException("-- ERROR: parsing failed. " + line);
		String[] fields = line.split("\\t");
		if ( fields.length != NUM_FIELDS )
			throw new RuntimeException("-- ERROR: parsing failed, expected " + 
					NUM_FIELDS + " fields recieved " + fields.length + " in " + line);
		snp_pos = fields[0].trim();
		strand = fields[1].trim();
		if ( !fields[2].trim().equals("?") )
			gene = fields[2].trim();
		if ( !fields[3].equals("?") )
			transcript = fields[3].trim();
		refa = fields[7].trim();
		type = fields[8].trim();
		nt1 = fields[9].trim().charAt(0);
		nt2 = fields[10].trim().charAt(0);
		if ( !fields[30].trim().equals("?") )
			dbrsid = fields[30].trim();  
	}
	
	public String strand;
	public String gene;

	public String transcript;
	public String refa;
	@XmlTransient
	public char nt1;
	@XmlTransient
	public char nt2;
	@XmlElement(name = "nt1")
	public void setNt1AsString(String s) {}
	public String getNt1AsString() {
		return String.valueOf(nt1);
	}
	@XmlElement(name = "nt2")
	public void setNt2AsString(String s) {}
	public String getNt2AsString() {
		return String.valueOf(nt2);
	}
	
	public String type;
	public String dbrsid;*/
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
