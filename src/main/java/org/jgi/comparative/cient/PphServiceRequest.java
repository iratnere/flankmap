package org.jgi.comparative.cient;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name="PphServiceRequest")
@Table(schema="rviewer", name="pph_request")
public class PphServiceRequest {
	
	@Id @GeneratedValue
    private int id;
	
	public PphServiceRequest() {
		this.status = Status.queued;
		created_at = Calendar.getInstance().getTime();
	}
	
	public int getId() {
		return id;
	}
	
	public String getSession_id() {
		return session_id;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public Date getCompleted_at() {
		return completed_at;
	}
	private String session_id;
	
	public enum Status {queued, in_process, completed, error};
	
	@Enumerated(EnumType.STRING)
	private Status status;
	
	public Status getStatus() {
		return status;
	}
	private Date created_at;
	private Date completed_at;
	
	
	@OneToMany(mappedBy = "pphRequest")
	private Set<PphSnp> snps = new HashSet<PphSnp>();

	public Set<PphSnp> getSnps() {
		return snps;
	}

	
}
