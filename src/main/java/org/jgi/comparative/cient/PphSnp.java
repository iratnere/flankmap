package org.jgi.comparative.cient;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity(name="PphSnp")
@Table(schema="rviewer", name="pph_snps")
public class PphSnp {

	@Id
    @GeneratedValue
    private int id;
	
	private String snp_pos;
	
	public PphSnp(){};
	public PphSnp(String snpPos, String refa, PphServiceRequest pphRequest,
			String dset) {
		super();
		snp_pos = snpPos;
		this.refa = refa;
		this.pphRequest = pphRequest;
		this.dset = dset;
	}
	private char strand;
	private String gene;
	public int getId() {
		return id;
	}
	public String getSnp_pos() {
		return snp_pos;
	}
	public char getStrand() {
		return strand;
	}
	public String getGene() {
		return gene;
	}
	public String getTranscript() {
		return transcript;
	}
	public String getRefa() {
		return refa;
	}
	public char getNt1() {
		return nt1;
	}
	public char getNt2() {
		return nt2;
	}
	public Type getType() {
		return type;
	}
	public String getDbrsid() {
		return dbrsid;
	}
	
	public String getDset() {
		return dset;
	}
	private String transcript;
	private String refa;
	private char nt1;
	private char nt2;
	
	public enum Type {missense, nonsense, coding_synon, intron, utr_3, utr_5};
	@Enumerated(EnumType.STRING)
	private Type type;
	
	
	private String dbrsid;
	@ManyToOne(cascade = CascadeType.PERSIST, fetch=FetchType.LAZY)
    @JoinColumn(name = "request_id")

	private PphServiceRequest pphRequest;
	public PphServiceRequest getPphRequest() {
		return pphRequest;
	}
	private String dset;
}
