package org.jgi.comparative.map;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jgi.comparative.db.model.GeneAnnotation;
import org.jgi.comparative.map.framework.EntityManagerKeeper;
import org.jgi.comparative.service.DataSetServiceImpl;


@SuppressWarnings("unchecked")
public abstract class Utils {
	public static final String MALFORMED_ACTION_URL = "MALFORMED_ACTION_URL";
	public static String processParameter(String paramValue, String paramName) {
		checkParameter(paramValue, paramName);
		return paramValue;
	}
	
	public static void checkParameter(String param, String paramName) {
		if ( param==null || param.isEmpty() )
			throw new RuntimeException("required parameter " + paramName + " is null or empty");
	}
	
	public static int dsetId(String dset) {
		return DataSetServiceImpl.getDsetIdFromParam(dset, false);
	}
	
	
	private static Map<String, Byte> chrIds;
	static {
		EntityManager em = EntityManagerKeeper.getSession();
		Session session = (Session) em.getDelegate();
		String queryString = "SELECT name, id FROM gp_common.chroms WHERE org=1";
		Query query = session.createSQLQuery(queryString);
		List result = query.list();
		chrIds = ArrayUtils.toMap(result.toArray());
	}
	
	public static int chrName2id(String chromName) {
		String s = chromName.substring(3);
		return chrIds.get(s);
	}
	
	public static String getDefaultGeneTableName(String annotDb) {
		GeneAnnotation annot = DataSetServiceImpl.getDefaultAnnotDbGenes( annotDb );
		if (annot == null )
			throw new RuntimeException("unable to find default gene table name for " + annotDb);
		return annot.getTableName();
	}
	
	public static final String mimetype = "text/plain";
	
	public static String sendGetRequest(String endpoint,
			String requestParameters) {
		if ( !endpoint.startsWith("http://") )
			return null;
		String result = null;
		try {
			String urlStr = endpoint;
			if (requestParameters != null && requestParameters.length() > 0) 
				urlStr += "?" + requestParameters;
			
			URL url = new URL(urlStr);
			URLConnection conn = url.openConnection();

			BufferedReader rd = new BufferedReader(new InputStreamReader(conn
					.getInputStream()));
			StringBuffer sb = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			rd.close();
			result = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static String rev(String str) {
		return new StringBuffer(str).reverse().toString();
	}
	
	public static String revComp(String str) {
		return StringUtils.replaceChars(StringUtils.reverse(str), "ATGC", "TACG");
	}

	public static String formatPosition(String name, int start, int end ) {
		return name + ":" + start + "-" + end;
	}

	public static String getSeq(String seqSvcUrl, String seqDbName, String dataset, int start, int end, int padding, int chromId) {
		String req = "server=" + seqDbName + "&db=adb_" + dataset + "&chrom="
				+ chromId + "&start=" + (start-padding) + "&end=" + (end+padding)  + "&uc=1&noheader=1&text=1";
		return sendGetRequest(seqSvcUrl, req);
	}
	
	public static String[] runSystemCommand(String cmd) {

		String sout = "";
		String errout = "";
		String[] result = new String[2];
		try {
			Process p = Runtime.getRuntime().exec(cmd);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			String s = "";
			while ((s = stdInput.readLine()) != null) {
				sout += s + "\n";
			};
			s = null;
			while ((s = stdError.readLine()) != null) {
				errout += s + "\n";
			};

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		result[0] = sout;
		result[1] = errout;
		return result;
	}

	
	
}
