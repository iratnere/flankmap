package org.jgi.comparative.map.flank;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.jgi.comparative.map.Common;
import org.jgi.comparative.map.Utils;
import org.jgi.comparative.model.client.Gene;

@Path("/map")
public class MapRequest {
	@Context UriInfo uriInfo;
	@Context static ServletContext context;

	private final static String geneP = "gene";
	private static final String flank_seq_3P = "flank_seq_3";
	private static final String flank_seq_5P = "flank_seq_5";

	private static String seqSvcUrl;
	private static String seqDbName;

	private static boolean initContextParams = false;;

	public static void initContextParams(String pseqSvcUrl, String pseqDbName) {
		seqSvcUrl = pseqSvcUrl;
		seqDbName = pseqDbName;
	}
	
	private static void initContextParams() {
		seqSvcUrl = context.getInitParameter("seqSvcUrl");
		seqDbName = context.getInitParameter("seqDbName");
		initContextParams = true;
	}
	
	private static final String mtype =  MediaType.APPLICATION_XML;

	@GET
	@Produces( { mtype })
	public Response doGet() {
		return doPost(uriInfo.getQueryParameters());
	}
	
	@POST
	@Produces( {  mtype })
	public Response doPost(MultivaluedMap<String, String> form) {
		if (!initContextParams)
			initContextParams();
		String gene = Utils.processParameter(form.getFirst(geneP), geneP);
		String flank_seq_5 = validateSeq(Utils.processParameter(form
				.getFirst(flank_seq_5P), flank_seq_5P), flank_seq_5P);
		String flank_seq_3 = validateSeq(Utils.processParameter(form
				.getFirst(flank_seq_3P), flank_seq_3P), flank_seq_3P);
		String dataset = Utils.processParameter(form.getFirst(Common.datasetP), Common.datasetP);
		String geneTable = form.getFirst("geneTable");
		if ( geneTable==null || geneTable.isEmpty() )
			geneTable = Utils.getDefaultGeneTableName(dataset);
		
		MapResult conf;
		String substitution = form.getFirst("sub");
		if ( substitution==null || substitution.isEmpty() )
			conf = new MapResult(dataset, geneTable, gene, flank_seq_5, flank_seq_3);
		else 
			conf = new MapResult(dataset, geneTable, gene, flank_seq_5, flank_seq_3,
					substitution, form.getFirst(Common.notify));
		return Response.ok(conf, mtype).header("Content-type",
					mtype).build();
	}

	private static String validateSeq(String seq, String param) {
		seq = seq.trim();
		if (!seq.matches("^[ATGC]+$"))
			throw new RuntimeException("found illegal character in " + param
					+ ", expecting (A,T,G,C)");
		return seq;
	}
	

	public static final int PADDING = 100;

	public static String getSeq(Gene gene, String dataset) {
		String req = "server=" + seqDbName + "&db=adb_" + dataset + "&chrom="
				+ gene.position.getChraffoldId() + "&start="
				+ (gene.position.getStart() - PADDING) + "&end="
				+ (gene.position.getEnd() + PADDING) + "&uc=1&noheader=1&text=1";

		return Utils.sendGetRequest(seqSvcUrl, req);
	}

}
