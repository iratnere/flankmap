package org.jgi.comparative.map.flank;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.jgi.comparative.db.model.DataSet;
import org.jgi.comparative.db.model.GeneAnnotation;
import org.jgi.comparative.map.Common;
import org.jgi.comparative.map.Utils;
import org.jgi.comparative.map.pph.PphResult;
import org.jgi.comparative.model.client.Gene;
import org.jgi.comparative.model.client.Strand;
import org.jgi.comparative.service.DataSetServiceImpl;
import org.jgi.comparative.service.SearchFeatureWorker;
import org.jgi.comparative.web.uiutils.client.NumberFormatter;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MapResult {
	String geneName;
	private static final String DELETION = "DELETION";

	public MapResult(String dataset, String geneTable, String geneName, 
			String upflank, String downflank) {
		this.dataset = dataset;
		this.geneTable = geneTable;
		this.geneName = geneName;
		this.flank_seq_5 = upflank;
		this.flank_seq_3 = downflank;
		process();
	}
	
	public MapResult(String dataset, String geneTable, String geneName, 
			String upflank, String downflank,
			String substitution, String email) {
		this.dataset = dataset;
		this.geneTable = geneTable;
		this.geneName = geneName;
		this.flank_seq_5 = upflank;
		this.flank_seq_3 = downflank;
		this.substitution = substitution;
		this.email = email;
		process();
		fetchPphData();
	}
	
	public MapResult(){};
	
	private void process() {
		DataSet dset = DataSetServiceImpl.getDataSetByFamilyName(dataset, false);
		GeneAnnotation annotation = DataSetServiceImpl.getDefaultAnnotDbGenes( dset.getAnnotDb() );
		List<Gene> genes = SearchFeatureWorker.findGenesByName(dset, annotation, geneName);
		JSONObject obj = new JSONObject();
		try {
			appendLog(obj);
			if (genes == null || genes.isEmpty()) {
				obj = new JSONObject();
				obj.put("gene", Common.NOT_FOUND);
				appendLog(obj);
			} else {
				int start = Integer.MAX_VALUE;
				int end = Integer.MIN_VALUE;
				Set<String> chrnames = new HashSet<String>();
				Set<String> strands = new HashSet<String>();
				for (Gene gene : genes) {
					obj = new JSONObject();
					chrnames.add(gene.position.getName());
					if (gene.position.getStart() < start)
						start = gene.position.getStart();
					if (gene.position.getEnd() > end)
						end = gene.position.getEnd();
					obj.put("gene", gene);
					appendLog(obj);
				}
				if (chrnames.size() > 1)
					throw new RuntimeException(
							"found genes located on different chromosomes!");
				if (strands.size() > 1)
					throw new RuntimeException(
							"found genes located on different strands!");
				Gene agene = genes.get(0);
				agene.position.setInterval(start, end);
				findHit(agene);
			}

		} catch (JSONException e) {
			throw new RuntimeException(e.getCause());
		}
	}
	
	private void fetchPphData() {
		pph_data = new PphResult(dataset, ref_position + " " + substitution.replace('>', '/'), email);
	}
	
	@XmlElement
	PphResult pph_data;
	
	String flank_seq_5;
	String flank_seq_3;

	String ref_position = Common.NOT_FOUND;
	public String ref_sequence = Common.NOT_FOUND;
	String dataset;
	String geneTable;
	String substitution;
	private String email;
	
	@XmlTransient
	JSONArray log = new JSONArray();

	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		try {
			obj.put("dataset", dataset);
			obj.put("geneTable", geneTable);
			obj.put("gene", geneName);
			obj.put("flank_seq_5", flank_seq_5);
			obj.put("flank_seq_3", flank_seq_3);
			obj.put("log", log);
			obj.put("ref_position", ref_position);
			obj.put("ref_sequence", ref_sequence);
		} catch (JSONException e) {
			throw new RuntimeException(e.getCause());
		}
		return obj;
	}

	public void appendLog(Object str) {
		log.put(str);
	}
	
	public void findHit(Gene gene) {
		JSONObject obj = new JSONObject();
		try {
			final boolean negstrand = gene.strand == Strand.NEGATIVE;
			final boolean posstrand = !negstrand;
			
			String seq_5 = posstrand ? flank_seq_5 : Utils.revComp(flank_seq_3);
			String seq_3 = posstrand ? flank_seq_3 : Utils.revComp(flank_seq_5);
			String seq = MapRequest.getSeq(gene, dataset);
			assert seq!=null;
			int genomeStart = gene.position.getStart() - MapRequest.PADDING;
			int seq5start = seq.indexOf(seq_5);
			boolean found5 = seq5start != -1;
			String hit5 = frmtmsg(found5, genomeStart, seq5start, seq_5
					.length());
			int seq3start = seq.indexOf(seq_3);
			boolean found3 = seq3start != -1;
			String hit3 = frmtmsg(found3, genomeStart, seq3start, seq_3
					.length());
			
			if ( negstrand ) {
				String tmp = hit3;
				hit3 = hit5;
				hit5= tmp;
			}
			
			obj.put("search_interval", gene.position.toUI());
			obj.put("flank_seq_5", hit5);
			obj.put("flank_seq_3", hit3);
			
			if (found5 && found3) {
				int seq5end = seq5start + seq_5.length() - 1;
				if (seq3start - 1 <= seq5end)
					ref_sequence = DELETION;
				else {
					String seqm = seq.substring(seq5end + 1, seq3start);
					if ( negstrand )
						seqm = Utils.revComp(seqm);
					int gs5end = genomeStart + seq5start + seq_5.length();
					int gs3start = genomeStart + seq3start - 1;
					
					ref_position = org.jgi.comparative.map.Utils.formatPosition(gene.position.getName(), gs5end, gs3start);
					ref_sequence = seqm;
					
				}
			}
			appendLog(obj);
		} catch (JSONException e) {
			throw new RuntimeException(e.getCause());
		}
	}
	
	
	private static String frmtmsg(boolean found, int genomeStart, int seqstart,
			int seqlength) {
		String hit = "";
		if (found) {
			int start = genomeStart + seqstart;
			int end = start + seqlength -1;
			hit = Common.FOUND + " at " + NumberFormatter.format(start) 
			+ "-" + NumberFormatter.format(end);
		} else
			hit = Common.NOT_FOUND;
		return hit;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	
}
