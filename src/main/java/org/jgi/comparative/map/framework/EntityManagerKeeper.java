package org.jgi.comparative.map.framework;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerKeeper {

	private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("flankmap-data");

	private static final ThreadLocal<EntityManager> threadSession = new ThreadLocal<EntityManager>();
	
	public static EntityManager getSession() {
		EntityManager entityManager = threadSession.get();
		if (entityManager == null) {
			entityManager = entityManagerFactory.createEntityManager();
			threadSession.set(entityManager);
		}
		return entityManager;
	}
	
	public static void closeSession() {
		EntityManager entityManager = threadSession.get();
		threadSession.set(null);
		if (entityManager != null && entityManager.isOpen())
			entityManager.close();
	}
	
	
}
