package org.jgi.comparative.map.framework;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


public class RequestFilter implements Filter {

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		try {
			chain.doFilter(request, response);
		} finally {
			org.jgi.comparative.framework.EntityManagerKeeper.closeSession();
			EntityManagerKeeper.closeSession();
		}

	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		org.jgi.comparative.framework.EntityManagerKeeper.getSession();
		EntityManagerKeeper.getSession();
	}

}