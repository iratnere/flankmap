package org.jgi.comparative.map.gene;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.jgi.comparative.map.Common;
import org.jgi.comparative.map.Utils;

@Path("/map/gene")
public class GeneRequest {
	@Context UriInfo uriInfo;
	@Context static ServletContext context;

	private final static String expressionP = "name";
	public static String seqSvcUrl;
	public static String seqDbName;

	private static boolean initContextParams = false;;

	public static void initContextParams(String pseqSvcUrl, String pseqDbName) {
		seqSvcUrl = pseqSvcUrl;
		seqDbName = pseqDbName;
	}
	
	private static void initContextParams() {
		seqSvcUrl = context.getInitParameter("seqSvcUrl");
		seqDbName = context.getInitParameter("seqDbName");
		initContextParams = true;
	}

	private final static String mtype = MediaType.APPLICATION_XML;

	@GET
	@Produces( { mtype })
	public Response doGet() {
		return doPost(uriInfo.getQueryParameters());
	}
	
	@POST
	@Produces( { mtype })
	public Response doPost(MultivaluedMap<String, String> form) {
		if (!initContextParams)
			initContextParams();
		String expression = Utils.processParameter(form.getFirst(expressionP), expressionP);
		String dataset = Utils.processParameter(form.getFirst(Common.datasetP), Common.datasetP);
		String geneTable = form.getFirst("geneTable");
		GeneResult conf = new GeneResult(dataset, geneTable, expression, form.getFirst(Common.notify));
		return Response.ok(conf, mtype).header("Content-type", mtype).build();
	}


}
