package org.jgi.comparative.map.gene;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.jgi.comparative.cient.Enhancer;
import org.jgi.comparative.db.model.DataSet;
import org.jgi.comparative.db.model.GeneAnnotation;
import org.jgi.comparative.db.model.impl.ColoredFeature;
import org.jgi.comparative.map.Utils;
import org.jgi.comparative.map.pph.PphResult;
import org.jgi.comparative.model.client.ChraffoldUI;
import org.jgi.comparative.model.client.Contig;
import org.jgi.comparative.model.client.Gene;
import org.jgi.comparative.model.client.Strand;
import org.jgi.comparative.service.ChraffoldServiceImpl;
import org.jgi.comparative.service.DataSetServiceImpl;
import org.jgi.comparative.service.SNPProvider;
import org.jgi.comparative.service.SearchFeatureWorker;
import org.jgi.comparative.web.uiutils.client.Pair;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GeneResult {

	public Contig contig;
	private String email;
	
	private void process() {
		parseExpression();
		if ( !isParsed() )
			return;
		
		if ( genomicexpression ) {
			setGenomicRefPositionBase();
			fetchData();
			return;
		}
		
		findGenes();
		if (!uniqueGeneFound())
			return;
		
		setRefPositionBase();
		seqSeqChange();
		fetchData();
	}
	
	private void fetchData() {
		fetchEnchancerData();
		fetchPphData();
	}
	
	private void fetchEnchancerData() {
		if (dataset.equals("hg19")) {
			enhancer = new Enhancer(Utils.chrName2id(chrName), refStart);
			if (enhancer.url == null)
				enhancer = null;
		}
	}

	private void fetchPphData() {
		pph_data = new PphResult(dataset, ref_position + " " + substitution.replace('>', '/'), email);
	}
	
	@XmlElement
	PphResult pph_data;
	
	@XmlElement
	Enhancer enhancer;
	
	public GeneResult(String dataset, String geneTable, String expression, String email) {
		this.dataset = dataset;
		this.geneTable = geneTable;
		this.expression = expression;
		this.email = email;
		process();
	}
	
	private int getChraffoldId() {
		ChraffoldUI chraffold = ChraffoldServiceImpl.getChromByName(contig.chrom, (Integer)getDataSet().getId(), false);
		return chraffold.getChraffoldId();
	}
	
	private void setGenomicRefPositionBase() {
		DataSet dset = DataSetServiceImpl.getDataSetByFamilyName(dataset, false);
		GeneAnnotation annotation = DataSetServiceImpl.getDefaultAnnotDbGenes( dset.getAnnotDb() );
		assert contigName!=null && !contigName.isEmpty();
		contig = SearchFeatureWorker.findContig(dset, annotation, contigName);
		if (contig == null)
			return;
		chrName = contig.chrom;
		refStart = contig.chromStart + cdLocation - 1;
		ref_position = chrName + ":" + refStart;
		int padding = 0;
		ref_base = Utils.getSeq(GeneRequest.seqSvcUrl, GeneRequest.seqDbName, dataset, refStart, refStart, padding, getChraffoldId());
	}

	public boolean uniqueGeneFound() {
		if ( genes == null || genes.size() !=1 ) 
				return false;
		if ( geneexpression )
			return true;
		
		assert mrnaexpression && mrna_version != null;
		GeneUI gene = genes.get(0);
		return gene.getMrna_version().equals(mrna_version);
		
	}
	
	public GeneResult(){};


	public String dataset;
	public String geneTableName;
	
	public List<GeneUI> genes;
	
	public String ref_position;
	public String ref_base;
	@XmlTransient
	private int cdLocation;
	public String geneName;
	
	public String contigName;
	
	private Short mrna_version;
	@XmlTransient
	private Integer offset;
	public String seqChange;
	public String comment;
	public String substitution;
	@XmlTransient
	private Integer exon;
	@XmlTransient
	private boolean isIntron;
	@XmlTransient
	private boolean isExon;
	private String expression;
	
	@XmlTransient
	private boolean parsed;
	public boolean isParsed() {
		return parsed;
	}

	@XmlTransient
	private boolean geneexpression;
	@XmlTransient
	private boolean genomicexpression;
	@XmlTransient
	private boolean mrnaexpression;
	@XmlTransient
	public int refStart;
	@XmlTransient
	public String chrName;
	@XmlTransient
	private String geneTable;
	
	private static final String ERROR_MESSAGE = " -- can't process the expression -- ";
	
	private void setRefPositionBase() {
		GeneUI gene = getGene();
		String order = geneOnNegativeStrand() ? "desc" : "asc";
		if ( geneTable==null || geneTable.isEmpty() )
			geneTable = Utils.getDefaultGeneTableName(dataset);
		geneTableName = "adb_" + dataset + "." + geneTable;
		List<ColoredFeature> features = SNPProvider.provideFeatures(geneTableName, gene.gene.gene_id, order);
		if ( features.isEmpty() )
			return;
		ColoredFeature feature = findFeature(features);
		if (feature == null)
			return;
		refStart = findRefPosition(feature);
		chrName = gene.gene.getPosition().getName();
		ref_position = chrName + ":" + refStart;
		setRef_base(refStart);
	}
	
	private void setRef_base(int refStart){
		GeneUI gene = getGene();
		int chromId = gene.gene.position.getChraffoldId();
		int padding = 0;
		ref_base = Utils.getSeq(GeneRequest.seqSvcUrl, GeneRequest.seqDbName, dataset, refStart, refStart, padding, chromId);
		if ( geneOnNegativeStrand() )
			ref_base = Utils.revComp(ref_base);
	}
	
	private GeneUI getGene() {
		assert uniqueGeneFound();
		return genes.get(0);
	}
	
	private DataSet getDataSet(){
		return DataSetServiceImpl.getDataSetByFamilyName(dataset, false);
	}
	
	public void findGenes() {
		DataSet dset = getDataSet();
		GeneAnnotation annotation = DataSetServiceImpl.getDefaultAnnotDbGenes( dset.getAnnotDb() );
		List<Gene> pgenes;
		if ( geneexpression )
			pgenes = SearchFeatureWorker.findGenesByName(dset, annotation, geneName );
		else {
			assert mrnaexpression && mrna_version!=null;
			pgenes = SearchFeatureWorker.findGenesByMrnaAccessNumber(dset, annotation, geneName, mrna_version);
		}
		if ( pgenes == null || pgenes.isEmpty() )
			return;
		this.genes = new ArrayList<GeneUI>(pgenes.size());
		for (Gene gene:pgenes)
			this.genes.add(new GeneUI(gene));
	}
	
	public void parseExpression() {
		Matcher geneMatcher = GeneResult.genePattern.matcher(expression);
		Matcher genomicMatcher = GeneResult.genomicPattern.matcher(expression);
		Matcher mrnaMatcher = GeneResult.mrnaPattern.matcher(expression);
		geneexpression = geneMatcher.find();
		genomicexpression = genomicMatcher.find();
		mrnaexpression = mrnaMatcher.find();
		Matcher matcher = null;
		if ( geneexpression ) {
			matcher = geneMatcher;
			geneName = matcher.group(2);
			cdLocation = Integer.valueOf(matcher.group(3));
		} else if ( genomicexpression ) {
			matcher = genomicMatcher;
			contigName = matcher.group(2) + "." + matcher.group(3);
			cdLocation = Integer.valueOf(matcher.group(4));
		} else if ( mrnaexpression ) { 
			matcher = mrnaMatcher;
			geneName = matcher.group(2);
			mrna_version = Short.valueOf( matcher.group(3));
			cdLocation = Integer.valueOf(matcher.group(4));
		} else {
			comment = ERROR_MESSAGE + expression;
			return;
		}
		
		Matcher subMatcher = subPattern.matcher(expression);
		boolean isSubs = subMatcher.find();
		if ( !isSubs ) {
			comment = ERROR_MESSAGE + expression;
			return;
		}
		substitution = subMatcher.group(1);
		seqChange = "substitution " + substitution;
		Matcher iMatcher = iPattern.matcher(expression);
		isIntron = iMatcher.find();
		isExon = !isIntron;
		if (isIntron) {
			String off = iMatcher.group(1);
			if (off.startsWith("+"))
				off = off.substring(1);
			offset = Integer.valueOf(off);
		} else {
			offset = cdLocation;
		}
		parsed = true;
	}
	
	private void seqSeqChange() {
		if (isIntron) {
			String dir = offset < 0 ? "before" : "after";
			seqChange += ", location = " + offset + " in intron " + dir
					+ " exon " + exon + " at position " + cdLocation;
		} else {
			seqChange += ", in exon " + exon + " at position " + offset;
		}
	}
	
	class CDS {
		Pair<Integer, Integer> exon;
		public CDS(Pair<Integer, Integer> exon, int findex) {
			super();
			this.exon = exon;
			this.findex = findex;
		}
		int findex;
		
		@Override
		public String toString() {
			return findex + ":" + exon.first + "-" + exon.second;
		}
	}
	
	private static Comparator<CDS> startsComp = new Comparator<CDS>() {
		@Override
		public int compare(CDS o1, CDS o2) {
			if ( o1.exon.first < o2.exon.first )
				return -1;
			if ( o1.exon.first > o2.exon.first )
				return 1;
			return 0;
		}
	};
	
	private static Comparator<CDS> endsComp = new Comparator<CDS>() {
		@Override
		public int compare(CDS o1, CDS o2) {
			if ( o1.exon.second < o2.exon.second )
				return -1;
			if ( o1.exon.second > o2.exon.second )
				return 1;
			return 0;
		}
	};
	
	public boolean geneOnNegativeStrand() {
		return getGene().gene.strand == Strand.NEGATIVE;
	}
	
	public int findRefPosition(ColoredFeature f) {
		assert f != null;
		GeneUI gene = getGene();
		int subPos;
		if ( geneOnNegativeStrand() ) {
			int estart = f.getEnd();
			if (isIntron && offset > 0) 
				estart = f.getStart(); 
			subPos = estart - offset;
		} else {
			int estart = f.getStart();
			if (isIntron && offset > 0) 
				estart = f.getEnd(); 
			subPos = estart + offset;
		}
		return gene.gene.start() + subPos - 1;
	}
	
	public ColoredFeature findFeature(List<ColoredFeature> features) {
		List<CDS> exons = new ArrayList<CDS>();
		int es = 0;
		for (int fi = 0; fi< features.size(); fi++) {
			ColoredFeature f = features.get(fi);
			char ftype = (char)f.getNumbers();
			assert ftype == 'U' || ftype == 'E'; 
			boolean isUTR = ftype == 'U';
			if ( isUTR )
				continue;
			int end = f.getEnd() - f.getStart() + 1;
			int start = 1;
			if (es > 0) {
				end += exons.get(es-1).exon.second;
				start = exons.get(es-1).exon.second + 1;
			}
			exons.add(new CDS(new Pair<Integer, Integer>(start, end), fi));
			es++;
		}
		
		Comparator<CDS> comp =  (isIntron && offset < 0) ? startsComp : endsComp;
		
		int index = Collections.binarySearch(exons, new CDS(new Pair<Integer, Integer>(cdLocation, cdLocation),0), comp);
		
		if ( index < 0 ) {
			if (isIntron)
				return null;
			index = -1 - index;
		}
		if ( index == exons.size() )
			return null;
		exon = index + 1;
		if (isExon)
			offset =  cdLocation - exons.get(index).exon.first;
		
		return features.get(exons.get(index).findex);
	}
	
	public static Pattern iPattern = Pattern.compile("([\\-+]\\d+).+$");
	private static final String variation = "\\.(\\d+))(.+)$";
	private static final String cvariation = ":c" + variation;
	private static final String gvariation = ":g" + variation;
	public static Pattern genePattern = Pattern.compile("(^(\\w+)" + cvariation);
	public static Pattern mrnaPattern = Pattern.compile("(^(NM_\\d+)\\.(\\d+)" + cvariation);
	public static Pattern genomicPattern = Pattern.compile("(^(NT_\\d+)\\.(\\d+)" + gvariation);
	public static Pattern subPattern = Pattern.compile("([ATGC]>[ATGC])$");
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
