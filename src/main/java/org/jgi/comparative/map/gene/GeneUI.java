package org.jgi.comparative.map.gene;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.jgi.comparative.model.client.Gene;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)

public class GeneUI {
	
	public GeneUI(){}
	
	@XmlTransient
	public Gene gene;
	
	public GeneUI(Gene gene) {
		this.gene = gene;
	}

	public String getPosition() {
		return gene.position.toSimpleString();
	}
	
	public String getDescription() {
		return gene.description;
	}
	
	public String getStrand() {
		return "" + gene.strand;
	}
	
	public String getName() {
		return gene.name;
	}
	
	public String getProt_acc() {
		return "" + gene.prot_acc;
	}
	
	public String getMrna_acc() {
		return "" + gene.mrna_acc;
	}
	
	public String getOmim_id() {
		return "" + gene.omim_id;
	}
	
	
	public Short getMrna_version() {
		return gene.mrna_version;
	}

	public void setMrna_version(Short mrnaVersion){};

	public void setStrand(String c){};
	public void setPosition(String s){};
	public void setDescription(String s){};
	public void setMrna_acc(String s){};
	public void setProt_acc(String s){};
	public void setOmim_id(String s){};
	public void setName(String s){};
}
