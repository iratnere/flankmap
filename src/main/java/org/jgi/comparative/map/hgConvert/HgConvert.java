package org.jgi.comparative.map.hgConvert;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.jgi.comparative.map.Utils;

@Path("/hgConvert")
public class HgConvert {
	@Context UriInfo uriInfo;
	private boolean initContextParams;
	private static String rundir = "/tmp";
	private static String liftOverHome = "/home/analysisCGP/data/CGP/CGP2/bin/liftover";
	private static String liftover = "liftOver";
	@Context static ServletContext context;

	private static final String mtype =  MediaType.TEXT_HTML;
	private static final String sourceRefP = "sourceRef";
	private static final String destRefP = "destRef";
	private static final String locationP = "location";
	
	@GET
	@Produces( { mtype })
	public Response doGet() {
		return doPost(uriInfo.getQueryParameters());
	}
	
	@POST
	@Produces( { mtype })
	public Response doPost(MultivaluedMap<String, String> form) {
		if ( !initContextParams )
			initContextParams();
		String sourceRef = Utils.processParameter(form.getFirst(sourceRefP), sourceRefP);
		String destRef = Utils.processParameter(form.getFirst(destRefP), destRefP);
		List<String> locations = form.get(locationP);
		if ( locations == null )
			throw new RuntimeException("required parameter " + locationP + " is null or empty");
		ResponseBuilder rb = Response.ok(LiftOverService.runCmd(rundir, liftOverHome, liftover, sourceRef, destRef, locations), 
					mtype).header("Content-type",
					mtype);
		return rb.build();
	}

	private void initContextParams() {
		rundir = context.getInitParameter("rundir");
		liftOverHome = context.getInitParameter("liftOverHome");
		liftover = context.getInitParameter("liftover");
		initContextParams = true;
	}

}
