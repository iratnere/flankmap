package org.jgi.comparative.map.hgConvert;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.List;

public class LiftOverService {

	private static String generateUniqueProcessId(String rundir, String prefix) {
		boolean found = false;
		String result = null;
		do {
			result = prefix + Long.toHexString(System.currentTimeMillis());
			File stdOutFile = new File(rundir + "/" + result + ".out");
			found = stdOutFile.exists();
			if ( found ) {
				try {
					Thread.sleep(1L);
				} catch (InterruptedException e) {
					// Ignore exception
				}
			}
		} while ( found );
		return result;
	}
	
	private final static String sep = "<br/>";
	
	public static String runCmd(String rundir, String liftOverHome, String cmd, String sourceRef, String destRef, List<String> locations) {
		//-positions fin ~/liftover/hg18ToHg19.over.chain fout unMapped
		String chain = sourceRef.toLowerCase() + "To" + 
			Character.toUpperCase(destRef.charAt(0)) + destRef.substring(1) 
			+ ".over.chain";
		String pid =  generateUniqueProcessId(rundir, chain + "-");
		String fin = rundir + "/" + pid + ".in";
		String fout = rundir + "/" + pid + ".out";
		Writer output = null;
		try {
			output = new BufferedWriter(new FileWriter(fin));
			for (String location : locations)
				output.write(location + "\n");
			output.close();
		} catch (IOException e1) {
			throw new RuntimeException(e1);
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
				}
			}
		}
		
		BufferedReader input = null;
		
		String[] command = {liftOverHome + "/" + cmd, 
				"-positions", fin, liftOverHome + "/" + chain, fout, "unMapped"};
		System.out.println("Executing" );
		for (String str: command)
			System.out.print(" " + str);
		System.out.println();
		String[] environment = {};
		try {
			Process p = Runtime.getRuntime().exec(command, environment, new File(rundir));
			System.out.println("Process started: " + p);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			String s;
			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
			}
			
			String es;

			while ((es = stdError.readLine()) != null) {
				System.err.println(es);
			}
			
			input = new BufferedReader(new FileReader(
					fout));
			StringBuilder sb = new StringBuilder();
			String so;
			while ((so = input.readLine()) != null) {
				sb.append(so + sep);
			}
			return sb.toString();
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
				}
			}
		}
	}
}
