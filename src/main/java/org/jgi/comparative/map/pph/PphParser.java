package org.jgi.comparative.map.pph;

public interface PphParser {
	void parse(String line);
	void init(String line);
}
