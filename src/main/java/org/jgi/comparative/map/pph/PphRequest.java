package org.jgi.comparative.map.pph;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.jgi.comparative.map.Common;
import org.jgi.comparative.map.Utils;

@Path("/map/pph")
public class PphRequest {
	@Context UriInfo uriInfo;
	@Context static ServletContext context;
	
	//default values
	static String endPointUrl = "http://genetics.bwh.harvard.edu";
	static String pphcgiUrl =  endPointUrl + "/cgi-bin/ggi/ggi2.cgi";
	static String pphUrl =  endPointUrl + "/ggi/pph2";
	public static String enhancerSchema = "idb_dev"; 
	public static String enhancerUrl = "http://enhancer.lbl.gov/cgi-bin/imagedb3.pl";
	private final static String mtype = MediaType.APPLICATION_XML;
	private final static String snp_param = "snp";

	private static boolean initContextParams = false;
	
	
	private static void initContextParams() {
		assert context != null;
		endPointUrl = context.getInitParameter("endPointUrl");
		pphcgiUrl = endPointUrl + context.getInitParameter("pphcgiUrl");
		pphUrl = endPointUrl + context.getInitParameter("pphUrl");
		enhancerUrl = context.getInitParameter("enhancerUrl");
		enhancerSchema = context.getInitParameter("enhancerSchema");
		initContextParams = true;
	}
	
	@GET
	@Produces( { mtype })
	public Response doGet() {
		return doPost(uriInfo.getQueryParameters());
	}
	
	@POST
	@Produces( { mtype })
	public Response doPost(MultivaluedMap<String, String> form) {
		if (!initContextParams)
			initContextParams();
		String sid = form.getFirst("sid");
		if ( sid !=null && !sid.isEmpty() ) {
			PphResult conf = new PphResult(sid);
			return Response.ok(conf, mtype).header("Content-type",
						mtype).build();
		}
		
		String dataset = Utils.processParameter(form.getFirst(Common.datasetP), Common.datasetP);
		List<String> snps = form.get(snp_param);
		if (snps==null || snps.isEmpty())
			throw new RuntimeException("required parameter " + snp_param + " is null or empty");
		PphResult conf = new PphResult(dataset, snps, form.getFirst(Common.notify));
		return Response.ok(conf, mtype).header("Content-type",
					mtype).build();
	}

}
