package org.jgi.comparative.map.pph;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.jgi.comparative.cient.PolyPhenOutput;
import org.jgi.comparative.cient.PphAnnot;
import org.jgi.comparative.service.PolyphenProvider;
import org.jgi.comparative.web.uiutils.client.StringUtils;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PphResult {
	
	String dataset;
	List<String> names;
	public List<String> names_for_request;

	public PphResult(String dataset, String name, String email) {
		List<String> names = new ArrayList<String>(1);
		names.add(name);
		init(dataset, names, email);
	}
	
	private void init(String dataset, List<String> names, String email) {
		this.dataset = dataset;
		this.names = names;
		this.email = email;
		Collections.sort(names);
		String pphtablename = providePolyphenTableName(dataset);
		if ( pphtablename != null) {
			pph_preds = PolyphenProvider.providePolyPhenPredictions(
				pphtablename, names);
		}
		
		names_for_request = getNamesForRequest();
		
		if ( names_for_request != null ) 
			sendPphRequest();
	}
	
	public PphResult(String dataset, List<String> names, String email) {
		init(dataset, names, email);
	}
	
	private static String data = "_ggi_project=PPHWeb2" +
		"&_ggi_origin=query&_ggi_target_pipeline=1&MODELNAME=HumVar";
	
	private String email;
	
	
	
	private void parseResource(String url, PphParser parser) {
		try {
			InputStream is = new URL(url).openStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = reader.readLine();
			assert line!= null;
			if (line.startsWith("#"))
				parser.init(line.substring(1));
			else 
				parser.parse(line);
			while ((line = reader.readLine()) != null) {
				parser.parse(line);
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			if (e instanceof java.io.FileNotFoundException)
				return;
			throw new RuntimeException(e);
		}
	}
	
	public static final Map<Integer, String> initHeaderMap(String header) {
		Map<Integer, String>  hmap = new HashMap<Integer, String>();
		String[] fields = header.split("\\t");
		int key = 0;
		for (String value: fields) 
			hmap.put(key++, value.trim());
		return hmap;
	}
	
	
	@XmlTransient
	private PphParser annotParser = new PphParser() {
		
		@Override
		public void parse(String line) {
			if ( !line.startsWith("#") )
				pph_annots.add(new PphAnnot(line, hmap));
		}

		Map<Integer, String> hmap;
		@Override
		public void init(String line) {
			hmap = initHeaderMap(line);
		}
	};
	
	@XmlTransient
	private PphParser predictParser = new PphParser() {
		
		@Override
		public void parse(String line) {
			if ( !line.startsWith("#") )
				pph_preds.add(new PolyPhenOutput(line, hmap));
		}

		Map<Integer, String> hmap;
		@Override
		public void init(String line) {
			hmap = initHeaderMap(line);
		}
	};
	
	@XmlTransient
	private PphParser pphLogsParser = new PphParser() {
		
		@Override
		public void parse(String line) {
			if ( line.contains("ERROR:") || line.contains("WARNING:"))
				pph_logs.add(line);
		}

		@Override
		public void init(String line) {
			// TODO Auto-generated method stub
			
		}
	};
	
	private void fetchData() {
		assert sid != null && !sid.isEmpty();
		
		String url = PphRequest.pphUrl + "/" + sid + "/1";
		parseResource(url + "/started.txt", new PphParser() {
			@Override
			public void parse(String line) {
				started_at = line.replaceAll("^\\w+", "").trim();

			}

			@Override
			public void init(String line) {
				// TODO Auto-generated method stub
				
			}
		});
		
		parseResource(url + "/completed.txt", new PphParser() {
			@Override
			public void parse(String line) {
				completed_at = line.replaceAll("^\\w+", "").trim();;
			}

			@Override
			public void init(String line) {
				// TODO Auto-generated method stub
				
			}
		});
		
		if ( completed_at != null ) {
			parseResource(url + "/pph2-snps.txt", annotParser);
			parseResource(url + "/pph2-short.txt", predictParser);
			parseResource(url + "/pph2-log.txt", pphLogsParser);
		}
	}
	
	private String sid;
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}

	String started_at;
	String completed_at;
	
	
	private void sendPphRequest() {
		String reqdata = data + "&UCSCDB=" + dataset;
		try {
			if ( email !=null )
				reqdata += "&NOTIFYME="
					+ URLEncoder.encode(email, "UTF-8");
			reqdata += "&_ggi_batch=" + 
				URLEncoder.encode(StringUtils.join(names_for_request, '\n'), "UTF-8");
			System.out.println("sendPphRequest: " + reqdata);
			URL url = new URL(PphRequest.pphcgiUrl);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(conn
					.getOutputStream());
			wr.write(reqdata);
			wr.flush();
			wr.close();
			sid = getCookieParam(conn, "Set-Cookie", "polyphenweb2");
			if ( sid == null )
				sid ="-- ERROR: submission to PolyPhen failed. " + 
						PphRequest.pphcgiUrl +
						" " + reqdata;
		} catch (Exception e) {
			ByteArrayOutputStream ba = new ByteArrayOutputStream();
			e.printStackTrace(new PrintStream(ba));
			sid ="-- ERROR: submission to PolyPhen failed. " + 
			PphRequest.pphcgiUrl +
			" " + reqdata + "\n" + ba.toString().substring(1, 300);
		}
		
	}
	
	private static String getCookieParam(URLConnection conn, String headerName, String cookieName) {
		String cheader = conn.getHeaderField(headerName);
		if ( cheader == null )
			return null;
		String[] cookies = cheader.split(";");
		String pvalue = null;
		if  ( cookies.length == 0 )
			return null;
		for (String cookie: cookies) {
			String nv[] = cookie.split("=");
			if (nv[0].equals(cookieName))
				pvalue = nv[1];
		}
		return pvalue;
	}

	public static String providePolyphenTableName(String dataset) {
		if ( !dataset.equals("hg18") && !dataset.equals("hg19"))
			return null;
		return "rviewer.pph";
	}
	
	public static String providePolyphenDupTableName(String dataset) {
		if ( !dataset.equals("hg18") && !dataset.equals("hg19"))
			return null;
		return "rviewer.pph_duplicates";
	}

	public PphResult(){};

	public PphResult(String sid) {
		this.sid = sid;
		fetchData();
	}

	@XmlElement
	List<PolyPhenOutput> pph_preds = new ArrayList<PolyPhenOutput>();
	
	@XmlElement
	List<String> pph_logs = new ArrayList<String>();
	
	@XmlElement
	List<PphAnnot> pph_annots = new ArrayList<PphAnnot>();
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
	
	private List<String> getNamesForRequest() {
		if (pph_preds == null || pph_preds.isEmpty())
			return names;
		if (pph_preds.size() == names.size())
			return null;
		List<String> result = new ArrayList<String>();
		for (String name:names) {
			if ( Collections.binarySearch(pph_preds, name) < 0 )
				result.add(name);
		}
		return result;
	}

}
