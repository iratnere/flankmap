package org.jgi.comparative.map.snp;

import java.awt.image.RenderedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.jgi.comparative.db.model.impl.Feature;
import org.jgi.comparative.map.Utils;
import org.jgi.comparative.model.client.Position;
import org.jgi.comparative.service.SNPProvider;
import org.jgi.comparative.service.SNPProvider.NamedStart;
import org.jgi.comparative.utils.ServerConstants;

@Path("/map/snp/image")
public class SnpImageRequest {
	@Context UriInfo uriInfo;
	@Context static ServletContext context;

	private final static String positionP = "position";
	private final static String snpP = "snp";
	private final static String widthP = "width";
	public final static String mtype = ServerConstants.CONTENT_TYPE_PNG;
	
	@GET
	@Produces( { mtype })
	public Response doGet() {
		return doPost(uriInfo.getQueryParameters());
	}
	
	
	private static String sep = ",";
	
	public static NamedStart featureFrom(String param) {
		//abc,123
		final String ERROR_MSG = "cannot parse a parameter: " + param;
		String[] nv = Position.parsePair(sep, param, ERROR_MSG);
		return new NamedStart( Integer.parseInt(nv[1]), nv[0] );
	}
	
	private static final int  def_label_width = 118;
	
	@POST
	@Produces( { mtype })
	public Response doPost(MultivaluedMap<String, String> form) {
		String pos = Utils.processParameter(form.getFirst(positionP), positionP);
		Position position = new Position(pos);
		List<String> snps = form.get(snpP);
		if ( snps == null || snps.isEmpty() )
			throw new RuntimeException("required parameter 'snp' is null or empty");
		int width = Integer.parseInt(Utils.processParameter(form.getFirst(widthP), widthP));
		List<SNPProvider.NamedStart> nstarts = new ArrayList<NamedStart>(snps.size());
		for (String param:snps) {
			nstarts.add (featureFrom(param) );
		}
		Collections.sort(nstarts);
		boolean shownames = Boolean.parseBoolean( form.getFirst("shownames") );
		List<Feature> fs = SNPProvider.getSnpDensity(position, nstarts, width, shownames);
		String lwidth = form.getFirst("label_width");
		int label_width = lwidth == null || lwidth.isEmpty() ? def_label_width
				: Integer.parseInt(lwidth);
		RenderedImage image =  SNPProvider.buildImage(position, fs, width, label_width);

		ResponseBuilder rb = Response.ok(image, mtype).header("Content-type",
					mtype);
		return rb.build();
	}
}
