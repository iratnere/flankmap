package org.jgi.comparative.map.snp;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.jgi.comparative.map.Common;
import org.jgi.comparative.map.Utils;

@Path("/map/snp")
public class SnpRequest {
	@Context UriInfo uriInfo;
	@Context static ServletContext context;

	private final static String geneP = "name";

	
	@GET
	@Produces( { MediaType.APPLICATION_XML })
	public Response doGet() {
		return doPost(uriInfo.getQueryParameters());
	}
	
	@POST
	@Produces( { MediaType.APPLICATION_XML })
	public Response doPost(MultivaluedMap<String, String> form) {
		String snpName = Utils.processParameter(form.getFirst(geneP), geneP);
		String dataset = Utils.processParameter(form.getFirst(Common.datasetP), Common.datasetP);
		SnpResult conf = new SnpResult(dataset, snpName, form.getFirst(Common.notify));
		String mtype = MediaType.APPLICATION_XML;
		ResponseBuilder rb = Response.ok(conf, mtype).header("Content-type",
					mtype);
		return rb.build();
	}

}
