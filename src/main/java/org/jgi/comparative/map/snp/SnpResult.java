package org.jgi.comparative.map.snp;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.jgi.comparative.db.model.Snp;
import org.jgi.comparative.map.pph.PphResult;
import org.jgi.comparative.service.SNPProvider;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SnpResult {
	String dataset;
	String snpName;
	@SuppressWarnings("unused")
	private String snpTable;
	

	public SnpResult(String dataset, String snpName, String email) {
		this.dataset = dataset;
		this.snpName = snpName;
		snpTable = SNPProvider.provideSnpTableName(dataset);
		fetchData();
		pph_data = new PphResult(dataset, snpName, email);
	}
	
	public SnpResult(){};

	@XmlElement
	private List<SnpUI> snps = new ArrayList<SnpUI>();
	
	@XmlElement
	PphResult pph_data;
	
	
	private void fetchData() {
		List<Snp> dbsnps = SNPProvider.provideSnps(snpName, dataset);
		if ( dbsnps == null || dbsnps.isEmpty() )
			return;
		for (Snp snp:dbsnps)
			snps.add(new SnpUI(snp, dataset));
		
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
