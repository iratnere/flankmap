package org.jgi.comparative.map.snp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.jgi.comparative.cient.Enhancer;
import org.jgi.comparative.db.model.PolyPhen;
import org.jgi.comparative.db.model.Snp;
import org.jgi.comparative.map.Utils;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class SnpUI implements Snp {
	
	@XmlTransient
	private Snp snp;
	
	public SnpUI(){};
	
	public SnpUI(Snp snp, String dataset) {
		super();
		this.snp = snp;
		if (dataset.equals("hg19")) {
			enhancer = new Enhancer(Utils.chrName2id(snp.getChromName()), snp.getChromStart());
			if (enhancer.url == null)
				enhancer = null;
		}
	}

	@XmlElement
	Enhancer enhancer;

	public String getRef_position() {
		return Utils.formatPosition(snp.getChromName(), snp.getChromStart(), snp.getChromEnd());
	}
	
	public String getRef_strand() {
		return "" + snp.getStrand();
	}
	
	public void setRef_strand(String c){};
	public void setRef_position(String s){};
	public void setFunction(String s){};
	public void setObserved(String s){};
	

	@Override
	public String getClassName() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getFunction() {
		return snp.getFunc();
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getLocType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getMolType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getObserved() {
		return snp.getObserved();
	}

	@Override
	public PolyPhen getPolyPhen() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStyleName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValid() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Object toJSONObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String tooltip() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int compareTo(Integer o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getChromEnd() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getChromName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getChromStart() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getScore() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public char getStrand() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public String toString() {
		String ret = getRef_position() + " " + 
		getRef_strand() + " " + 
		getFunction(); 
		if ( enhancer != null )
			ret += " " + enhancer.toString();
		return ret;
	}

	@Override
	public String getFunc() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
