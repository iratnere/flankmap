package org.jgi.comparative.service;

import java.lang.reflect.Field;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.jgi.comparative.cient.PolyPhenOutput;
import org.jgi.comparative.map.framework.EntityManagerKeeper;


public class PolyphenProvider {
	@SuppressWarnings("unchecked")
	public static List<PolyPhenOutput> providePolyPhenPredictions(String tableName,
			List<String> names) {
		
		if (tableName == null)
			return null;
		
		if (names ==null || names.isEmpty())
			return null;
		
		String select = bldSelect();
		String innames = "";
		for (String name: names) {
			Matcher rsMatcher = rsPattern.matcher(name);
			if ( !rsMatcher.find() )
				continue;
			if ( innames.isEmpty() )
				innames += "'" + name + "'";
			else 
				innames += ",'" + name + "'";
		}

		//String innames = "'" + StringUtils.join(names, "','") + "'";
		
		if (innames.isEmpty())
			return null;
		
		String where = " WHERE snp_id IN (" + innames  + ") ";
		String query =  select + " FROM " + tableName + where + " ORDER BY snp_id"; 

		EntityManager em = EntityManagerKeeper.getSession();
		Session session = (Session) em.getDelegate();

		SQLQuery sqlQuery = session.createSQLQuery(query);
		sqlQuery.setResultTransformer(Transformers.aliasToBean(PolyPhenOutput.class));
		return sqlQuery.list();
	}
	
	
	@SuppressWarnings("unchecked")
	private static String bldSelect() {
		String select = "SELECT ";
		try {
			Class cls = Class.forName("org.jgi.comparative.cient.PolyPhenOutput");
			Field fieldlist[] 
			                = cls.getDeclaredFields();
			for (int i=0; i< fieldlist.length; i++) {
				String name = fieldlist[i].getName();
				//if (fname.equals("duplicate_rsid"))
					//continue;
				select += i==0? name : "," + name;
			}
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		return select;
	}
	
	public static Pattern rsPattern = Pattern.compile("^rs\\d+$");


	public static List<PolyPhenOutput> providePolyPhenDuplicates(
			String pphduptablename, List<String> namesForRequest) {
		// TODO Auto-generated method stub
		return null;
	}
}
