package org.jgi.comparative;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.regex.Matcher;

import org.jgi.comparative.db.model.DataSet;
import org.jgi.comparative.db.model.impl.ColoredFeature;
import org.jgi.comparative.map.Common;
import org.jgi.comparative.map.Utils;
import org.jgi.comparative.map.gene.GeneRequest;
import org.jgi.comparative.map.gene.GeneResult;
import org.jgi.comparative.map.gene.GeneUI;
import org.jgi.comparative.model.client.ChraffoldUI;
import org.jgi.comparative.service.ChraffoldServiceImpl;
import org.jgi.comparative.service.DataSetServiceImpl;
import org.jgi.comparative.service.SNPProvider;
import org.junit.Test;

public class MapGeneTest {
	
	final static String seqSvcUrl = "http://hazelton.lbl.gov/cgi-bin/gp_get_seq";
	final static String seqDbName = "hazelton";
	static void initContext() 	{
		GeneRequest.initContextParams(seqSvcUrl, seqDbName);
	}

	//
	@Test
	public void testProcessRequest() {
		initContext();
		String base = "hg19";
		String geneName = "CFTR";
		String[] names = { "c.254G>A", "c.350G>A", "c.443T>C", "c.489+1G>T",
				"c.579+1G>T", "c.948delT", "c.1000C>T", "c.1040G>C",
				"c.1364C>A", "c.1519_1521delATC", "c.1521_1523delCTT",
				"c.1585-1G>A", "c.1624G>T", "c.1652G>A", "c.1657C>T",
				"c.1679G>C", "c.1766+1G>A", "c.2052delA", "c.2657+5G>A",
				"c.2988+1G>T", "c.3437delC", "c.3484C>T", "c.3718-2477C>T",
				"c.3846G>A", "c.3909C>G" };
		for ( String pname:names ) {
			sendRequest(base, geneName + ":" + pname);
		}
		//sendRequest(base, "CFTR:c.489+1G>T");
		//sendRequest(base, "NT_006316.16:g.15888590C>T");
		
		
	}
	
	private void sendRequest(String base, String name) {
		String geneTable = Utils.getDefaultGeneTableName(base);
		String ename;
		try {
			ename = URLEncoder.encode(name, "UTF8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		System.out.println(name);
		System.out.println(ename);
		GeneResult conf = new GeneResult(base, geneTable, name, Common.email);
		assertNotNull(conf);
		if ( !conf.isParsed() )
			return;
		System.out.println(conf.ref_base + " " + name + "," 
				+ conf.ref_position 
				+ " " + conf.substitution.replace('>', '/'));
		char base_char = conf.substitution.charAt(0);
		
		assertTrue(conf.ref_base!=null && conf.ref_base.charAt(0) == base_char);
		System.out.println(conf);
	}
	
	
	@Test
	public void testRE() {
		
		/*
	MYH7:c.2606G>A                substitution in exon at position 2606                                       NM_000257.24:c.2606G>A
    EGFR:c.2237_2255del          deletion in exon at positions 2237_2255                                  NM_005228.3:c.2237_2255del
    MYBPC3:c.2149-1G>A        substitution in intron (location == 1 bp before exon at 2149)        NM_000256.29:c.2149-1G>A
    TNNI3:c.821+46C>T            substitution in intron (location == 46 bp after exon at 821)           NM_000363.3:c.821+46C>T
		 */
		 
		String[] strs = {"NT_006316.16:g.15888590C>T", "MYH7:c.2606G>A", "EGFR:c.2237_2255del", 
				"MYBPC3:c.2149-1G>A", "TNNI3:c.821+46C>T",
				"NM_005228.1:c.2155G>T"};
		//String[] strs = {"NM_005228.1:c.2155G>T"};
		
		for (String str: strs) {
			System.out.println(str);
			Matcher geneMatcher = GeneResult.genePattern.matcher(str);
			Matcher mrnaMatcher = GeneResult.mrnaPattern.matcher(str);
			boolean genefound = geneMatcher.find();
			boolean mrnafound = mrnaMatcher.find();
			assertTrue( genefound || mrnafound);
			Matcher matcher = genefound ? geneMatcher : mrnaMatcher;
			/*System.out.print(" ");
			for (int i = 1; i <= startMatcher.groupCount(); i++  )
				System.out.print(" " + startMatcher.group(i));*/
			String gene = matcher.group(2);
			if (mrnafound) {
				int version = Integer.valueOf( matcher.group(3));
				gene += "." + version;
			}
			int exonLoc = Integer.valueOf(genefound? matcher.group(3): matcher.group(4));
			Matcher subMatcher = GeneResult.subPattern.matcher(str);
			
			System.out.print("gene: " + gene);
			boolean isSubs = subMatcher.find();
			if ( !isSubs ) {
				System.out.println(" -- can't process the expression -- " + str);
				continue;
			}
			
			String subs = subMatcher.group(1);
			System.out.print(" substitution " + subs);
			Matcher iMatcher = GeneResult.iPattern.matcher(str);
			boolean isIntron = iMatcher.find();
			if ( isIntron ) {
				String off = iMatcher.group(1);
				if (off.startsWith("+"))
					off = off.substring(1);
				int offset = Integer.valueOf(off);
				String dir = offset < 0 ? "before" : "after"; 
				System.out.print(", location == " + offset + " in intron " + dir + " exon at " + exonLoc);
			} else {
				System.out.print(", in exon at " + exonLoc);
			}
			System.out.println();
		}
	}
	
	
	@Test
	public void testFindGenomicPosition() {
		initContext();
		String base = "hg19";
		String name = "NT_006316.16:g.15888590C>T";
		GeneResult conf = new GeneResult(base, null, name, Common.email);
		assertTrue(conf.isParsed());
		assertFalse(conf.contig == null);
		int padding = 0;
		DataSet dset = DataSetServiceImpl.getDataSetByFamilyName(base, false);
		ChraffoldUI chraffold = ChraffoldServiceImpl.getChromByName(conf.contig.chrom, (Integer)dset.getId(), false);
		String seq = Utils.getSeq(seqSvcUrl, seqDbName, base, conf.refStart, conf.refStart, padding, chraffold.getChraffoldId());
		char ref_base = seq.charAt(padding);
		char base_char = conf.substitution.charAt(0);
		assertTrue(ref_base == base_char);
		System.out.println(seq + " "  + name
				+ "," + conf.ref_position
				+ " " + conf.substitution.replace('>', '/'));
		System.out.println(conf);
	}
	
	@Test
	public void testFindPosition() {
		initContext();
		String base = "hg18";
		String geneName = "CFTR";
		String[] names = { "c.254G>A", "c.350G>A", "c.443T>C", "c.489+1G>T",
				"c.579+1G>T", "c.948delT", "c.1000C>T", "c.1040G>C",
				"c.1364C>A", "c.1519_1521delATC", "c.1521_1523delCTT",
				"c.1585-1G>A", "c.1624G>T", "c.1652G>A", "c.1657C>T",
				"c.1679G>C", "c.1766+1G>A", "c.2052delA", "c.2657+5G>A",
				"c.2988+1G>T", "c.3437delC", "c.3484C>T", "c.3718-2477C>T",
				"c.3846G>A", "c.3909C>G" };
		for ( String pname:names ) {
			process(base, geneName + ":" + pname);
		}
		
		//String name = "EGFR:c.2155G>T";
		//process(base, "EGFR:c.2155G>T");
		/*String[] names = { "NM_201284.1", "NM_201283.1", "NM_005228.3", "NM_201282.1" };
		for ( String pname:names ) {
			process(base, pname + ":c.2155G>T");
		}*/
		
		/*String[] names = {"PRKAG2:c.298G>A", "TPM1:c.241-9T>C", "MYBPC3:c.3233G>A", "MYBPC3:c.2274C>T",
				"MYBPC3:c.1566G>A", "MYBPC3:c.532G>A", "MYBPC3:c.459delC",
				"NM_000256.3:c.682G>A" };*/
		
		/*for ( String name:names ) 
			process(base, name);*/
	}
	
	
	private void process(String base, String name) {
		String geneTable = Utils.getDefaultGeneTableName(base);
		GeneResult conf = new GeneResult(base, geneTable, name, Common.email);
		if ( !conf.isParsed() )
			return;
		
		if ( !conf.uniqueGeneFound() )
			return;
		
		GeneUI gene = conf.genes.get(0);
		GeneRequest.initContextParams(seqSvcUrl, seqDbName);
		String order = conf.geneOnNegativeStrand() ? "desc" : "asc";
		List<ColoredFeature> features = SNPProvider.provideFeatures(conf.geneTableName,
				gene.gene.gene_id, order);
		assertFalse(features.isEmpty());
		ColoredFeature feature = conf.findFeature(features);
		if (feature == null)
			return;
		
		int refStart = conf.findRefPosition(feature);
		int chromId = gene.gene.position.getChraffoldId();
		int padding = 0;
		String seq = Utils.getSeq(seqSvcUrl, seqDbName, base, refStart, refStart, padding, chromId);
		char ref_base = seq.charAt(padding);
		if ( conf.geneOnNegativeStrand() ) {
			ref_base = Utils.revComp("" + ref_base).charAt(0);
			seq = Utils.revComp(seq);
		}
		char base_char = conf.substitution.charAt(0);
		assertTrue(ref_base == base_char);
		System.out.println(seq + " "  + name
				+ "," + gene.gene.getPosition().getName() + ":" + refStart
				+ " " + conf.substitution.replace('>', '/'));
		// System.out.println(seq + " " + refStart + " " + ++conf.exon +
		// " " + name + " " + conf.seqChange);
	}

}
