package org.jgi.comparative;

import static org.junit.Assert.*;

import java.util.Random;

import org.jgi.comparative.db.model.GeneAnnotation;
import org.jgi.comparative.map.Common;
import org.jgi.comparative.map.flank.MapRequest;
import org.jgi.comparative.map.flank.MapResult;
import org.jgi.comparative.service.DataSetServiceImpl;
import org.junit.Test;

public class MapServiceTest {

	private static String seqSvcUrl = "http://hazelton.lbl.gov/cgi-bin/gp_get_seq";
	private static String seqDbName = "hazelton";
	private static String dataset = "hg18";

	@Test
	public void testRE() {
		String str = "CCTGTGAGGCCCAAGACCAGCCCC";
		assertTrue(str.matches("^[ATGC]+$"));
	}
	
	
	@Test
	public void testProcessRequest() {
		MapRequest.initContextParams(seqSvcUrl, seqDbName);
		/*sendRequest("PRKAG2", "CCTGTGAGGCCCAAGACCAGCCCC",
				"GCTCTCCCAAAACCGTGTTCCCGT", "G");
		
		sendRequest("TPM1", "CCAACTCTGAAATGCTTTTCACTC",
				"CTACCTAGGCTGAAGCCGACGTAG", "T");*/
		
		sendRequest("MYBPC3", "AGGATCTCCGGGTGACTGACGCCT",
				"GGGTCTTAATGTGGCTCTGGAGTG", "G");
		
		/*sendRequest("MYBPC3", "GGTCACAGTGAAGAACCCTGTGGG",
		"GAGGACCAGGTCAACCTCACAGTC", "C");
		
		sendRequest("MYBPC3", "CAACGAGGCCATGCTGGAGGACGC",
		"GGGCACTATGCACTGTGCACTAGC", "G");
		
		sendRequest("MYBPC3", "GGCAGCATCACCTTCTCAGCCCGC",
		"TGGCCGGCGCCAGCCTCCTGAAGC", "G");
		
		sendRequest("MYBPC3", "TACCCCTGGAGCCCCCGATGACCC",
		"ATTGGCCTCTTCGTGATGCGGCCA", "C");
		
		sendRequest("MYBPC3", "TATCTGTTCGAGCTGCACATCACC",
				"ATGCCCAGCCTGCCTTCACTGGCA", "G");*/
	}
	
	String bases = "ATGC";

	private void sendRequest(String gene, String fseq5, String fseq3, String expected_seq) {
		GeneAnnotation annot = DataSetServiceImpl.getDefaultAnnotDbGenes( dataset );
		assertNotNull(annot); 
		MapResult mapres = new MapResult(dataset, annot.getTableName(), gene, fseq5, fseq3);
		/*JSONObject result = MapRequest.processRequestAsJsonObject(mapres);
		assertNotNull(result);
		try {
			System.out.println(result.toString(4));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		try {
			String ref_seq = result.getString("ref_sequence");
			assertTrue(expected_seq.equals(ref_seq));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		System.out.print(mapres);
		String ref_seq = mapres.ref_sequence;
		assertTrue(expected_seq.equals(ref_seq));
		int np1 = bases.indexOf(ref_seq);
		int np2 = generator.nextInt( 4 );
		while (np2 == np1)
			np2 = generator.nextInt( 4 );
		char nt = bases.charAt(np2);
		String sub = ref_seq + ">" + nt;
		mapres = new MapResult(dataset, annot.getTableName(), gene, fseq5, fseq3,
				sub, Common.email);
		System.out.print(mapres);
	}
	
	Random generator = new Random();

}
