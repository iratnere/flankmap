package org.jgi.comparative;


import org.jgi.comparative.map.Common;
import org.jgi.comparative.map.snp.SnpResult;
import org.junit.Test;

public class MapSnpTest {
	
	@Test
	public void testProcessRequest() {
		sendRequest("hg19", "rs57615040");
		/*
		 * from snp130
'rs72477211', 'unknown'
'rs55874132', 'coding-synon'
'rs71224359', 'intron'
'rs61761323', 'coding-synon,intron'
		 */
	}
	
	private void sendRequest(String dataset, String snpName) {
		SnpResult mapres = new SnpResult(dataset, snpName, Common.email);
		System.out.print(mapres);
	}

}
