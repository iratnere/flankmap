package org.jgi.comparative;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.jgi.comparative.cient.PolyPhenOutput;
import org.jgi.comparative.cient.PphAnnot;
import org.jgi.comparative.cient.PphServiceRequest;
import org.jgi.comparative.cient.PphSnp;
import org.jgi.comparative.map.Common;
import org.jgi.comparative.map.framework.EntityManagerKeeper;
import org.jgi.comparative.map.pph.PphResult;
import org.jgi.comparative.service.PolyphenProvider;
import org.junit.Test;

public class PphTest {

	private static String dataset = "hg18";
	private final static String pphUrl = "http://genetics.bwh.harvard.edu/cgi-bin/ggi/ggi2.cgi";
	private static String data = "_ggi_project=PPHWeb2" +
		"&_ggi_origin=query&_ggi_target_pipeline=1&MODELNAME=HumVar&UCSCDB=" + dataset;
		 
	@Test
	public void testFindRequest() {
		EntityManager em = EntityManagerKeeper.getSession();
		PphServiceRequest req = em.find(PphServiceRequest.class, 1);
		assertNotNull(req);
		
	}
	
	@Test
	public void testPphServiceRequestObject() {
		EntityManager em = EntityManagerKeeper.getSession();
		EntityTransaction tx = em.getTransaction();
		PphServiceRequest req = new PphServiceRequest();
		em.persist(req);
		try {
			tx.begin();
			tx.commit();
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
		}
		int id = req.getId();
		assert id != 0;
		PphServiceRequest req1 = em.find(PphServiceRequest.class, id);
		assert req1 != null && req1.equals(req);
		em.remove(req);
		try {
			tx.begin();
			tx.commit();
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
		}
		
		PphServiceRequest req2 = em.find(PphServiceRequest.class, id);
		assert req2 == null;
		
		em.close();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testFindSnp() {
		EntityManager em = EntityManagerKeeper.getSession();
		EntityTransaction tx = em.getTransaction();
		PphServiceRequest req = new PphServiceRequest();
		List<PphSnp> snps = new ArrayList<PphSnp>();
		snps.add( new PphSnp("chr7:116936413", "G/A", req, dataset) );
		snps.add( new PphSnp("chr7:116958405", "G/T", req, dataset) );
		snps.add( new PphSnp("chr7:116961656", "G/T", req, dataset) );
		for (PphSnp snp: snps)
			em.persist(snp);
		try {
			tx.begin();
			tx.commit();
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
		}
		
		PphSnp snpa = snps.get(0);
		String query = "SELECT s FROM PphSnp s WHERE s.dset = '" + dataset +
		"' AND s.snp_pos = '" + snpa.getSnp_pos() + "' AND s.refa = '" + snpa.getRefa() + "'";
		Query q = em.createQuery(query);
		List<PphSnp> result = q.getResultList(); 
		assert result != null && !result.isEmpty();
		
		for (PphSnp snp: snps)
			em.remove(snp);
		em.remove(req);
		try {
			tx.begin();
			tx.commit();
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
		}
		em.close();
	}
	
	@Test
	public void testCreatePphSnps() {
		EntityManager em = EntityManagerKeeper.getSession();
		EntityTransaction tx = em.getTransaction();
		PphServiceRequest req = new PphServiceRequest();
		List<PphSnp> snps = new ArrayList<PphSnp>();
		snps.add( new PphSnp("chr7:116936413", "G/A", req, dataset) );
		snps.add( new PphSnp("chr7:116958405", "G/T", req, dataset) );
		for (PphSnp snp: snps)
			em.persist(snp);
		try {
			tx.begin();
			tx.commit();
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
		}
		PphSnp snpa = snps.get(0);
		int id = snpa.getId();
		assert id != 0;
		PphSnp snp1 = em.find(PphSnp.class, id);
		assert snp1 != null && snp1.equals(snpa);
		
		for (PphSnp snp: snps)
			em.remove(snp);
		em.remove(req);
		try {
			tx.begin();
			tx.commit();
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
		}
		
		PphSnp snp2 = em.find(PphSnp.class, id);
		assert snp2 == null;
		
		em.close();
	}
	
	
	/*private HTMLEditorKit.ParserCallback callback = new HTMLEditorKit.ParserCallback() {
		public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
			if (t == HTML.Tag.INPUT) {
				if (a.containsAttribute(Attribute.NAME, "sid")) {
					System.out.println("sid" + " -> "
							+ a.getAttribute(Attribute.VALUE));
				}
			}
		}
	};*/
	
	
	
	@Test
	public void testEncode() {
		/*String snps = "chr7:116936413 G/A" +
			"\nchr7:116958405 G/T" +
			"\nchr7:116961656 G/T";*/
		
		String snps = "hi there!";
		
		try {
			String es = URLEncoder.encode(snps, "UTF-8");
			System.out.println(es);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Test
	public void testGETRequest() {
		try {
			String query = "http://genetics.bwh.harvard.edu/ggi/pph2/" +
					"1d86e42794470ed6f85c6ca1058790e8fe277a61/";
			//query += "9/pph2-snps.txt";
			//query += "9/started.txt";
			query += "9/completed.txt";
			InputStream is = new URL(query).openStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private static String getCookieParam(URLConnection conn, String headerName, String cookieName) {
		String cheader = conn.getHeaderField(headerName);
		if ( cheader == null )
			return null;
		String[] cookies = cheader.split(";");
		String pvalue = null;
		if  ( cookies.length == 0 )
			return null;
		for (String cookie: cookies) {
			String nv[] = cookie.split("=");
			if (nv[0].equals(cookieName))
				pvalue = nv[1];
		}
		return pvalue;
	}
	
	@Test
	public void testPOSTRequest() {
		/*String snps = "chr7:116936413 G/A" +
			"\nchr7:116958405 G/T" +
			"\nchr7:116961656 G/T";*/
		
		String snps = "chr7:116936413 G/A";
		
		try {
			data += "&NOTIFYME="
					+ URLEncoder.encode("iratnere@lbl.gov", "UTF-8");
			data += "&_ggi_batch=" + URLEncoder.encode(snps, "UTF-8");
			System.out.println(pphUrl);
			System.out.println(data);
			URL url = new URL(pphUrl);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(conn
					.getOutputStream());
			wr.write(data);
			wr.flush();
			
			/*BufferedReader reader = new BufferedReader(new InputStreamReader(conn
					.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
			new ParserDelegator().parse(reader, callback, true);*/
			wr.close();
			String sid = getCookieParam(conn, "Set-Cookie", "polyphenweb2");
			assert sid != null;
			System.out.println("sid" + " -> " + sid);
			//reader.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Test
	public void testPphAnnot() {
		String pphout = "pph2-snps.txt";
		String sid = "a41f0bcc3de86beba12e56d0754ee9e353afafab";
		String query = "http://genetics.bwh.harvard.edu/ggi/pph2/" +
			sid + "/1/" + pphout;
		List<PphAnnot> annots = new ArrayList<PphAnnot>();
		Map<Integer, String> hmap;
		try {
			InputStream is = new URL(query).openStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = reader.readLine();
			assert line!= null;
			hmap = PphResult.initHeaderMap(line);
			while ((line = reader.readLine()) != null) {
				if (line.startsWith("#"))
					continue;
				annots.add(new PphAnnot(line, hmap));
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		assert !annots.isEmpty();
		System.out.println(annots);
	}
	
	@Test
	public void testPphStatusRequest() {
		String sid = "beb479adcbddaf57946b1ad7905bfb4788b45490";
		PphResult pphs = new PphResult(sid);
		System.out.println(pphs);
	}
	
	
	@Test
	public void testProcessRequest() {
		/*String query = "rs12184494,rs77819407,rs34754396,"
				+ "rs79137504,rs61744131,rs78131776,rs77292473,"
				+ "rs45475699,rs61752776,rs79340002";*/
		String query = "chr7:116961656 G/T, chr7:116936413 G/A," +
				"chr7:117015028 G/A, rs12184494, chr7:116958358 T/C, rs77819407, rs71245736";
		String[] snps = query.split(",");
		String urlQuery = "";
		try {
			for (int i=0; i< snps.length; i++) {
				snps[i] = snps[i].trim();
				urlQuery += "&snp=" + URLEncoder.encode(snps[i], "UTF8");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		System.out.println(urlQuery);
		
		List<String> names = Arrays.asList(snps);
		PphResult pphs = new PphResult(dataset, names, Common.email);
		System.out.println(pphs);
		String sid =  pphs.getSid();
		assertFalse(sid == null);
	}

	
	@Test
	public void testRE() {
		String str = "rs12184494";
		Pattern rsPattern = Pattern.compile("^rs\\d+$");
		Matcher rsMatcher = rsPattern.matcher(str);
		assertTrue(rsMatcher.find());
		rsMatcher = rsPattern.matcher(str+"_abc");
		assertFalse(rsMatcher.find());
	}
	
	@Test
	public void testProvidePolyPhenPredictions() {
		String query = "rs12184494,rs77819407,rs34754396," +
				"rs79137504,rs61744131,rs78131776,rs77292473," +
				"rs45475699,rs61752776,rs79340002,ku-ku";
		String[] snps = query.split(",");
		String urlQuery = "";
		for (String snp: snps) {
			try {
				urlQuery += "&snp=" + URLEncoder.encode(snp, "UTF8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		System.out.println(urlQuery);
		List<String> names =Arrays.asList(snps);
		String pphtablename = PphResult.providePolyphenTableName(dataset);
		if (pphtablename ==null)
			return;
		
		List<PolyPhenOutput> pphs = PolyphenProvider.providePolyPhenPredictions(
				pphtablename, names);
		if ( pphs.size() < names.size() ) {
			List<String> names_for_request = getNamesForRequest(pphs, names);
			String pphduptablename = PphResult.providePolyphenDupTableName(dataset);
			List<PolyPhenOutput> dups = PolyphenProvider.providePolyPhenDuplicates(
					pphduptablename, names_for_request);
			if ( dups!= null )
				pphs.addAll(dups);
		}
		assertFalse(pphs == null || pphs.isEmpty());
		//assertTrue(pphs.size() == snps.length);
		System.out.println(pphs);
	}
	
	private List<String> getNamesForRequest(List<PolyPhenOutput> pph_preds,
			List<String> names) {
		if (pph_preds == null || pph_preds.isEmpty())
			return names;
		if (pph_preds.size() == names.size())
			return null;
		List<String> result = new ArrayList<String>();
		for (String name:names) {
			if ( Collections.binarySearch(pph_preds, name) < 0 )
				result.add(name);
		}
		return result;
	}
	

}
