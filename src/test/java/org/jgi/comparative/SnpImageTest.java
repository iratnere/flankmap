package org.jgi.comparative;

import static org.junit.Assert.*;

import org.jgi.comparative.db.model.impl.Feature;
import org.jgi.comparative.map.snp.SnpImageRequest;
import org.jgi.comparative.model.client.Position;
import org.jgi.comparative.service.SNPProvider;
import org.jgi.comparative.service.SNPProvider.NamedStart;
import org.junit.Test;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;


public class SnpImageTest {
	
	
	private static String encodeParam(String param) {
		String enstr;
		try {
			enstr = URLEncoder.encode(param, "UTF8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return enstr;
	}
	
	@Test
	public void testSnpImage() {
		//String query = "width=900&position=chr1:100-500&snp=abc,323&snp=cde,133";
		/*String query = "width=750&position=chr7:55209001-55210183" +
				"&snp=NM_005228.3:c.2155G>T,55209201&snp=NM_005228.3:c.2259G>T,55209983" +
				"&shownames=true";*/
		String query = "shownames=true&width=900&position=chr7:55209001-55210185" +
		"&snp=NM_005228.3:c.2155G>T,55209002" +
		"&snp=NM_005228.3:c.2155G>C,55209003" +
		"&snp=NM_005228.3:c.2155G>T,55209203" +
		"&snp=NM_005228.3:c.2155G>T,55209204" +
		"&snp=NM_005228.3:c.2155G>T,55209204" +
		"&snp=NM_005228.3:c.2155G>T,55209207" +
		"&snp=NM_005228.3:c.2259G>T,55209983" +
		"&snp=NM_005228.3:c.2259G>T,55209984" +
		"&snp=NM_005228.3:c.2259G>T,55209985";
		
		//NM_005228.3:c.2155G>T   NM_005228.3:c.2259G>T   NM_005228.3:c.2625+6G>T
		System.out.println(query);
		Map<String, List<String>> pmap = parseQuery(query);
		assertNotNull(pmap);
		String pos = pmap.get("position").get(0);
		int width = Integer.parseInt(pmap.get("width").get(0));
		String encquery = "width=" + width;
		boolean shownames = pmap.get("shownames") !=null ?
				Boolean.parseBoolean(pmap.get("shownames").get(0)) : true;
		encquery += "&position=" + encodeParam(pos);
		Position position = new Position(pos);
		assertTrue( position!=null );
		List<String> snps = getUrlParameter(query, pmap, "snp");
		List<SNPProvider.NamedStart> nstarts = new ArrayList<NamedStart>(snps.size());
		for (String param:snps) {
			nstarts.add ( SnpImageRequest.featureFrom( param ) );
			encquery += "&snp=" + encodeParam(param);
		}
		Collections.sort(nstarts);
		System.out.println(encquery);
		List<Feature> fs = SNPProvider.getSnpDensity(position, nstarts, width, shownames);
		assertTrue( fs != null && !fs.isEmpty());
		for (Feature f : fs) 
			System.out.println(f);
		
		File file = new File("testSNPs.png");
		final int label_width = 118;
		RenderedImage image =  SNPProvider.buildImage(position, fs, width, label_width);
		try {
			ImageIO.write(image, "png", file);
		} catch (IOException e) {
			fail();
		}
	}
	
	private static List<String> getUrlParameter(String query, 
			Map<String, List<String>> pmap, String name){
		List<String> vals = pmap.get(name);
		if ( vals == null || vals.isEmpty() )
			throw new RuntimeException("malformed query string: " + query);
		return vals;
	}
	
	private static Map<String, List<String>> parseQuery(String query) {
		if (query == null || query.isEmpty())
			return null;
		String q = query.trim();
		String[] params = q.split("&");
		if (params.length == 0)
			return null;

		Map<String, List<String>> pmap = new HashMap<String, List<String>>();
		for (String s : params) {
			String[] nv = s.split("=");
			if (nv.length != 2)
				throw new RuntimeException("malformed query string: " + query);
			String name = nv[0];
			String value = nv[1];
			List<String> pvals = pmap.get(name);
			if (pvals == null)
				pvals = new ArrayList<String>();
			pvals.add(value);
			pmap.put(name, pvals);
		}
		return pmap;
	}
}
